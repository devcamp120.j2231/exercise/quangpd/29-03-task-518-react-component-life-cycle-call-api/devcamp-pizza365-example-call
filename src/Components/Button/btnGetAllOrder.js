import { Component } from "react";

class ButtonGetAllOrder extends Component{
    constructor(props){
        super(props)
    }

    getAllOrder = () =>{
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        fetch("http://203.171.20.210:8080/devcamp-pizza365/orders", requestOptions)
        .then(response => response.json())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
    }

    render(){
        return(
            <>
                <div>
                    <button onClick={this.getAllOrder} className="btn btn-danger">Get All Order</button>
                </div>
            </>
        )
    }
}

export default ButtonGetAllOrder