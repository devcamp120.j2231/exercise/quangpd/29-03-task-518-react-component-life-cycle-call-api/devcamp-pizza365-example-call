import { Button } from "bootstrap";
import { Component } from "react";

class ButtonCheckVoucher extends Component{
    constructor(props){
        super(props)
    }
    
    CheckVoucherById = () =>{
        var requestOptions ={
            method : 'GET',
            redirect : 'follow'
        }

        let VoucherId = "24864"

        fetch("http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + VoucherId,requestOptions)
            .then(response => response.json())
            .then(result => console.log(result))
            .catch(error => console.log(error))
    }

    render(){
        return(
            <>
                <div>
                    <button onClick={this.CheckVoucherById} className="btn btn-success">Check Voucher By Id </button>
                </div>
            </>
        )
    }
}

export default ButtonCheckVoucher