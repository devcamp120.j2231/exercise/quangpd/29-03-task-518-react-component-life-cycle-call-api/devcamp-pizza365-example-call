import { Component } from "react";

class ButtonGetDrinkList extends Component{
    constructor(props){
        super(props)
    }

    getDrinkList =() =>{
        var requestOptions = {
            method : 'GET',
            redirect : 'follow'
        }

        fetch("http://203.171.20.210:8080/devcamp-pizza365/drinks",requestOptions)
            .then(response => response.json())
            .then(result => console.log(result))
            .catch(error => console.log(error))
    }

    render(){
        return(
            <>
                <div>
                    <button onClick={this.getDrinkList} className="btn btn-danger">Get Drink List</button>
                </div>
            </>
        )
    }
}

export default ButtonGetDrinkList