import { Component } from "react";

class ButtonUpdateOrder extends Component{

    constructor(props){
        super(props)
    }

    UpdateOrder = () =>{
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "trangThai" : "confirmed"
        })

        var requestOptions ={
            method : 'PUT',
            header : myHeaders,
            body : raw,
            redirect : 'follow'
        }

        let orderId = 162404;

        fetch("http://203.171.20.210:8080/devcamp-pizza365/orders" + "/" + orderId,requestOptions)
            .then(response => response.json())
            .then(result => console.log(result))
            .catch(error => console.log(error)); 

    }
    render(){
        return(
            <>
                <div>
                    <button onClick={this.UpdateOrder} className="btn btn-info">Update Order</button>
                </div>
            </>
        )
    }
}

export default ButtonUpdateOrder