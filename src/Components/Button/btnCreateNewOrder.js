import { Component } from "react";

class ButtonCreateOrder extends Component{
    constructor(props){
        super(props)
    }

    fetchApi = async (url , requestOptions) =>{

        let response = await fetch(url , requestOptions);
        let data = await response.json();

        return data
    }


    CreateNewOrder = () =>{
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "orderCode": "JQT1z1lBGb",
            "kichCo": null,
            "duongKinh": null,
            "suon": 0,
            "salad": null,
            "loaiPizza": "hải sản",
            "idVourcher": null,
            "thanhTien": 0,
            "giamGia": 0,
            "idLoaiNuocUong": null,
            "soLuongNuoc": 0,
            "hoTen": "Phạm Đức Quang",
            "email": "quangpd0298@gmail.com",
            "soDienThoai": "0877979735",
            "diaChi": "usa",
            "loiNhan": "11h"
        });

        var requestOptions = {
            method : 'POST',
            headers : myHeaders,
            body : raw,
            redirect : 'follow'
        }

        fetch("http://203.171.20.210:8080/devcamp-pizza365/orders", requestOptions)
            .then(response => response.json())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));
    }

    render(){
        return(
            <>
                <div>
                    <button onClick={this.CreateNewOrder} className="btn btn-primary">Create New Order</button>
                </div>
            </>
        )
    }
}

export default ButtonCreateOrder