import "bootstrap/dist/css/bootstrap.min.css";
import ButtonGetAllOrder from "./Components/Button/btnGetAllOrder";
import ButtonCreateOrder from "./Components/Button/btnCreateNewOrder";
import ButtonGetOrderById from "./Components/Button/btnGetOrderById";
import ButtonUpdateOrder from "./Components/Button/btnUpdateOrder";
import ButtonCheckVoucher from "./Components/Button/btnCheckVoucher";
import ButtonGetDrinkList from "./Components/Button/btnGetDrinkList";

function App() {
  return (
    <div className="container">
      <div className="row">
        <div className="col-sm-12 mt-5">
          <h1 className="text-center">Example Call Pizza 365</h1>
        </div>

        <div className="row mt-5">
          <div className="col">
            <ButtonGetAllOrder/>
          </div>
          <div className="col">
            <ButtonCreateOrder/>
          </div>
          <div className="col">
            <ButtonGetOrderById/>
          </div>
          <div className="col">
            <ButtonUpdateOrder/>
          </div>
          <div className="col">
            <ButtonCheckVoucher/>
          </div>
          <div className="col">
            <ButtonGetDrinkList/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
